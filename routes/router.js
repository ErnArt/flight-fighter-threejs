const express = require('express');
const router = express.Router();

const indexController = require('../controllers/indexController');
const stageController = require('../controllers/stageController');

router.get('/', indexController.index);
router.get('/stage', stageController.stage);

module.exports = router;