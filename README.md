# Flight Fighter ThreeJS NodeJS

## Description

This project is a ThreeJS test project for my third year of Undergraduate in IT studies. I did take inspiration of retro Shoot'Em Up with ripped ressources from https://www.vg-resource.com to experiment and understand the framework. This is an evolution of my older Flight Fighter project as a NodeJS and Express app and using the CI/CD functionalities.

This project is made up of :
- the framework (ThreeJS) used to render 3D scenes using WebGL.
- the framework (Express) for routing our application.
- the framework (EJS), Embedded JavaScript templates for better modularity.
- the Javascript library (Howler) for more functionalities for audio objects.
- the Javascript library (dat.gui) used to display a GUI on our main window.
- the daemon (nodemon) for refreshing the application when a change is made.

## Dependencies

- npm

## Librairies installation

```sh
npm install
```

## Usage

```sh
npm run start

# Open your favorite browser
vivaldi localhost:3000
chrome localhost:3000
firefox localhost:3000
```

Or accessing this direct link : https://flightfighter.ernart.com.

## Input

- Up or Z : Going forward.
- Left or Q : Left rotation.
- Down or S : Turning back.
- Right or D : Right rotation.
- Left and Right Shift Key : Shot missiles.
- Mute button : To mute sound.