exports.stage = function(req, res)
{
	let stageLevel = req.query.level;
	console.log(stageLevel);
	res.render('../views/layoutPage.ejs', {title: 'Space', page: 'stage', level: stageLevel});
}